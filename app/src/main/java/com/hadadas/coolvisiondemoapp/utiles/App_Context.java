package com.hadadas.coolvisiondemoapp.utiles;

import android.app.Application;

import com.hadadas.coolvisiondemoapp.volley.MyVolley;


/**
 * Application class for init singletones. Used to ensure that MyVolley,AnalyticsSampleApp,gps is initialized. {@see MyVolley}
 * 
 *
 */
public class App_Context extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        
        init();
    }


    private void init() {
        MyVolley.init(this);
    }
    

}
