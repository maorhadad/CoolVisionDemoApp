package com.hadadas.coolvisiondemoapp.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.hadadas.coolvisiondemoapp.R;
import com.hadadas.coolvisiondemoapp.custom_views.MyImageView;
import com.hadadas.coolvisiondemoapp.pogos.Photo;
import com.hadadas.coolvisiondemoapp.volley.MyVolley;

import java.util.List;

/**
 * Created by Maor on 03/09/2015.
 */
public class ListRowAdapter extends ArrayAdapter<Photo> {

    private Context mContext;
    private LayoutInflater mInflater;
    private ImageLoader imageLoader;

    public ListRowAdapter(Context context, int resource, List<Photo> objects) {
        super(context, resource, objects);

        this.mContext = context;
        mInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = MyVolley.getImageLoader();
    }
    @Override
    public int getCount() {
        return super.getCount();
    }
    @Override
    public Photo getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Photo photo = getItem(position);
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.list_row_layout,
                    parent, false);

            holder = new ViewHolder();
            holder.tv_title = (TextView) convertView
                    .findViewById(R.id.tv_title);
            holder.tv_description = (TextView) convertView
                    .findViewById(R.id.tv_description);
            holder.tv_Url = (TextView) convertView.findViewById(R.id.tv_Url);
            holder.iv_image = (MyImageView) convertView.findViewById(R.id.iv_image);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tv_title.setId(position);
        holder.tv_description.setId(position);
        holder.tv_Url.setId(position);
        holder.iv_image.setId(position);

        if(photo.getTitle()!= null) {
            holder.tv_title.setText(photo.getTitle());
        }else {
            holder.tv_title.setVisibility(View.GONE);
        }
        if(photo.getDescription()!= null  ) {
            if(!photo.getDescription().isEmpty()) {
                holder.tv_description.setText(photo.getDescription());
            }else {
                holder.tv_description.setVisibility(View.GONE);
            }
        }else {
            holder.tv_description.setVisibility(View.GONE);
        }
        if(photo.getImg()!= null) {
            holder.tv_Url.setText(photo.getImg());
            holder.iv_image.setImageUrl(photo.getImg(),imageLoader);
            holder.iv_image.setVisibility(View.VISIBLE);

        }else {
            holder.tv_Url.setVisibility(View.GONE);
            holder.iv_image.setVisibility(View.GONE);
        }

        return convertView;
    }
    class ViewHolder {
        MyImageView iv_image;
        TextView tv_title,tv_description,tv_Url;

    }
}
