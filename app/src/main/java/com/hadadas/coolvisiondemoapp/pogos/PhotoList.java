package com.hadadas.coolvisiondemoapp.pogos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maor on 03/09/2015.
 */
public class PhotoList {

    private List<Photo> photo;

    public PhotoList (){
        this.photo = new ArrayList<Photo>();
    }
    public List<Photo> getPhoto() {
        return photo;
    }

    public void setPhoto(List<Photo> photo) {
        this.photo = photo;
    }
}
